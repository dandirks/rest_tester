# REST Tester

A quick and dirty way to test the four main HTTP verbs. To use, start the
Node.js application and point your browser at it. There is some configuration
at the top of the page that can be used to adjust how the testing happens.
